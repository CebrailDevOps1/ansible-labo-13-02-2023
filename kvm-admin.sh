#!/bin/bash

### Variables
request=$1
prefix=$2
first=$3
last=$4
delay=2

### Functions
clone_vm(){
  for suffix in $(seq $first $last)
  do
    template="$request"
    vm_name=$prefix$suffix
    virt-clone --original $template --name $vm_name --file /media/cebrail/Elements/DevOps/kvm/$vm_name.qcow2
    sleep $delay
  done
}

start_vm(){
  for suffix in $(seq $first $last)
  do
    vm_name=$prefix$suffix
    virsh start $vm_name
    sleep $delay
  done
}

stop_vm(){
  for suffix in $(seq $first $last)
  do
    vm_name=$prefix$suffix
    virsh shutdown $vm_name
    sleep $delay
  done
}

delete_vm(){
  for suffix in $(seq $first $last)
  do
    vm_name=$prefix$suffix
    virsh undefine --domain $vm_name --storage /media/cebrail/Elements/DevOps/kvm/$vm_name.qcow2
    sleep $delay
  done
}

### Main code
case $request in 
  debian11|rocky|ubuntu)
    clone_vm
    ;;
  start)
    start_vm
    ;;
  stop)
    stop_vm
    ;;
  delete)
    stop_vm
    delete_vm
    ;;
  *)
    echo ""
esac
